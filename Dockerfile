# Copyleft (c) March, 2022, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune yay" \
  description="Dune in yay" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-yay/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-yay" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG AUR_YAY="https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz"

RUN sudo pacman --needed --noconfirm --noprogressbar -Syuq && \
  mkdir -p ~/build && \
  cd ~/build && \
  curl -LO ${AUR_YAY} && \
  tar -xvf yay.tar.gz && \
  cd yay && \
  makepkg --noconfirm -si && \
  rm -rf ~/build ~/.cache /tmp/makepkg && \
  pacman -Qtdq | xargs -r sudo pacman --noconfirm -Rscn && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*
